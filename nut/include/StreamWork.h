#ifndef _StreamWork_H
#define _StreamWork_H
#include <dev/debug.h>
#ifdef ETHERNUT2
#include <dev/lanc111.h>
#else
#include <dev/nicrtl.h>
#endif
#include <dev/vs1001k.h>
//#include "lcdA.h"
#include <sys/version.h>
#include <sys/confnet.h>
#include <sys/heap.h>
#include <sys/bankmem.h>
#include <sys/thread.h>
#include <sys/timer.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <net/route.h>
#include <netinet/tcp.h>

#include <pro/dhcp.h>

#define QMUSICIP 	"93.187.10.33"
#define QMUSICPORT 	80
#define QMUSICURL	"/Qmusic_nl_live_96.mp3"
#define THREEFMIP	"145.58.53.152"
#define THREEFMPORT 80
#define THREEFMURL	"/3fm-bb-mp3"
#define PRCNLIP		"81.23.236.242"
#define PRCNLPORT	80
#define PRCNLURL	"/100pctnl.mp3"

//extern char* currentSong;
extern char songtitle[50];
int stopAlarm;

void stopStream(void);
int ConfigureLan(char *devname);
FILE* ConnectStation(TCPSOCKET *sock, u_long ip, u_short port, u_long *metaint);
int ProcessMetaData(FILE *stream);
void PlayMp3Stream(FILE *stream, u_long metaint);
void startStream(u_long, u_short, char *);
void alarmBeep(void);
void setGVolume(int volume);
void setBass(void);
void setLeftRightBalance(int left, int right);
void getLeftRightBalance(int* leftA, int* rightA);
void setBassEnhancer(int on);

#endif

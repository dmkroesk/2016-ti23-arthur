#include "rtc.h"
#include "alarm.h"
#include "EEPROMHandler.h"
#include <stdio.h>
#include "rtc.h"
#include "vs10xx.h"
#include "flash.h"
#include "StreamWork.h"
#include <time.h>

int threeTimes;
int wkAlarmDay;
int wkAlarmSet;
int wkAlarmDayFrom;
void setHardcodedAlarm(void);
void getHardcodedAlarm(void);
/*
*
*Use initAlarms after EEPROMHandler readEEPROM() function.
*/

void initAlarms()
{
	threeTimes = 0;
    setHardcodedAlarm();
    getHardcodedAlarm();
}

//Flag long
int	 flag1 = 143;
//Integer for alarm checker

void setHardcodedAlarm()
{
 //   printf("------------------------------------------------\n");
 //   printf("\nAlarm 1 is dagelijks alarm: \n");
 //   tm alarmTime;
 //   alarmTime.tm_sec = 00;
 //   alarmTime.tm_min = 10;
 //   alarmTime.tm_hour = 13;
	//alarmTime.tm_wday = 4;
 //   alarmTime.tm_mday = 9;
 //   //alarmTime.tm_mon = 17;
 //   //alarmTime.tm_year = 116;
 //   printf("Alarm time is: %02d:%02d:%02d\n", alarmTime.tm_hour, alarmTime.tm_min, alarmTime.tm_sec);
 //   //printf("Alarm date is: %02d.%02d.%04d\n", alarmTime.tm_mday, (alarmTime.tm_mon + 1), (alarmTime.tm_year + 1900));

 //   //Fill in alarm struct and set alarm
 //   setAlarm(&IMCconfig.alarm1, alarmTime, on, stream, "OK", 50);
 //   X12RtcSetAlarm(0, &IMCconfig.alarm1.dateTime, flag1);
	X12RtcSetAlarm(0, &IMCconfig.alarm1.dateTime, flag1);
}

void getHardcodedAlarm()
{
    //Recieve RTC time and display
    tm gmt;
    X12RtcGetAlarm(0, &gmt, &flag1);
    printf("\nSet Alarm: Date: %d-%d-2016 Time: %d:%d:%d \n", gmt.tm_mday, (gmt.tm_mon + 1), gmt.tm_hour, gmt.tm_min, gmt.tm_sec);
    printf("------------------------------------------------\n");
}


 void setAlarm(struct Alarm *alarm,tm dt, enum STATUS st, enum MODE md,char* str, int vol)
 {
     alarm->dateTime = dt;
     alarm->status = st;
     alarm->mode = md;
     alarm->stream = str;
     alarm->volume = vol;
	 alarm->isSet=1;
	 saveEEPROM();
 }

 void setVariableAlarm()
 {
     X12RtcSetAlarm(1, &IMCconfig.alarm2.dateTime, flag1);
 }

 struct _tm getVariableAlarm()
 {
     tm alarmTime;
     X12RtcGetAlarm(1, &alarmTime, &flag1);
     return alarmTime;
 }

int CheckAlarm1()
{
    tm alarmTime;
    X12RtcGetAlarm(0, &alarmTime, &flag1);
    tm currentTime;
    X12RtcGetClock(&currentTime);
    int alarm1Seconds = 0;
	alarm1Seconds = ((alarmTime.tm_wday - 80) * 86400)+ (alarmTime.tm_hour * 3600) + (alarmTime.tm_min * 60) + (alarmTime.tm_sec);
    int currentSeconds = 0;
    currentSeconds = ((currentTime.tm_wday) * 86400) +(currentTime.tm_hour * 3600) + (currentTime.tm_min * 60) + (currentTime.tm_sec);
	 if(currentSeconds >= (alarm1Seconds - 30) && currentSeconds <= (alarm1Seconds + 30))
    {
        return 1;
    }
	else
    {
        return 0;
    }
}
int CheckAlarm2()
{
    tm alarmTime;
    X12RtcGetAlarm(1, &alarmTime, &flag1);
    tm currentTime;
    X12RtcGetClock(&currentTime);
    int alarm1Seconds = 0;
    alarm1Seconds = ((alarmTime.tm_wday-80)* 86400) + (alarmTime.tm_hour * 3600) + (alarmTime.tm_min * 60) + (alarmTime.tm_sec);
    int currentSeconds = 0;
    currentSeconds = ((currentTime.tm_wday)* 86400)+(currentTime.tm_hour * 3600) + (currentTime.tm_min * 60) + (currentTime.tm_sec);
	printf("Alarm1 : %d \t %d:%d:%d: %d\n",alarm1Seconds,alarmTime.tm_hour,alarmTime.tm_min ,alarmTime.tm_sec, (alarmTime.tm_wday -80 ));
    printf("Current: %d \t %d:%d:%d: %d\n",currentSeconds,currentTime.tm_hour,currentTime.tm_min,currentTime.tm_sec, (currentTime.tm_wday));
   
    if(currentSeconds >= (alarm1Seconds - 30) && currentSeconds <= (alarm1Seconds + 30))
    {
        return 1;
    }
	else
    {
        return 0;
    }
}

void turnOffAlarm(int alarm1or2)
{
	tm alarm1TimeSetting;
	tm alarmTime1;
	printf("[Alarm] stop alarm \n");
		if (alarm1or2 == 1 && wkAlarmSet == 1)
		{
			X12RtcGetClock(&alarm1TimeSetting);
			X12RtcGetAlarm(0, &alarmTime1, &flag1);
			if (((alarmTime1.tm_wday - 80) < wkAlarmDay) & ((alarmTime1.tm_wday - 80) >= wkAlarmDayFrom))
			{
				printf("[Alarm] weekalarm set next day alarm1 \n");
				alarm1TimeSetting.tm_wday = (alarmTime1.tm_wday - 79);
				X12RtcSetAlarm(0, &alarm1TimeSetting, flag1);
				setAlarm(&IMCconfig.alarm1, alarm1TimeSetting, on, stream, "OK", 50);
			}
			else
			{
				printf("[Alarm] weekalarm finished \n");
				wkAlarmSet == 0;
			}
		}
		else if(alarm1or2 ==2 && wkAlarmSet == 1)
		{
			X12RtcGetClock(&alarm1TimeSetting);
			X12RtcGetAlarm(1, &alarmTime1, &flag1);

			if (((alarmTime1.tm_wday-80) < wkAlarmDay)& ((alarmTime1.tm_wday - 80) >= wkAlarmDayFrom))
			{
				printf("[Alarm] weekalarm set next day alarm2 \n");
				alarm1TimeSetting.tm_wday = (alarmTime1.tm_wday - 79);
				X12RtcSetAlarm(1, &alarm1TimeSetting, flag1);
				setAlarm(&IMCconfig.alarm2, alarm1TimeSetting, on, stream, "OK", 50);
			}
			else
			{
				printf("[Alarm] weekalarm finished \n");
				wkAlarmSet == 0;
			}
		}
		stopStream();
}

void weekAlarm(int day,int day2, int set)
{
	wkAlarmSet = set;
	wkAlarmDay = day;
	wkAlarmDayFrom = day2;
	printf("[Alarm] wk alarm set %d", set);
	printf("[Alarm] wk alarm day from %d", day2);
	printf("[Alarm] wk alarm day %d", day);
}
void snoozeAlarm(int snoozeTime, int alarm1or2)
{
	if (threeTimes < 3)
	{
		tm alarm1TimeSetting;
		int minSet;
		int hourSet;
		if(alarm1or2 ==1)
		{
			X12RtcGetClock(&alarm1TimeSetting);

			if (alarm1TimeSetting.tm_min + snoozeTime < 60)
			{
				minSet = alarm1TimeSetting.tm_min + snoozeTime;
				hourSet = alarm1TimeSetting.tm_hour;
			}
			else
			{
				minSet = (alarm1TimeSetting.tm_min + snoozeTime) - 60;
				hourSet = alarm1TimeSetting.tm_hour + 1;
			}
			tm alarmTime1 = { alarm1TimeSetting.tm_sec, minSet, hourSet, alarm1TimeSetting.tm_mday,alarm1TimeSetting.tm_mon,alarm1TimeSetting.tm_year ,alarm1TimeSetting.tm_wday,alarm1TimeSetting.tm_yday,alarm1TimeSetting.tm_isdst };
			X12RtcSetAlarm(0, &alarmTime1, flag1);
			printf("[Alarm] alarm 1 snoozed \n");
		}
		else
		{
			X12RtcGetClock(&alarm1TimeSetting);

			if (alarm1TimeSetting.tm_min + snoozeTime < 60)
			{
				minSet = alarm1TimeSetting.tm_min + snoozeTime;
				hourSet = alarm1TimeSetting.tm_hour;
			}
			else
			{
				minSet = (alarm1TimeSetting.tm_min + snoozeTime) - 60;
				hourSet = alarm1TimeSetting.tm_hour + 1;
			}
			tm alarmTime1 = { alarm1TimeSetting.tm_sec, minSet, hourSet, alarm1TimeSetting.tm_mday,alarm1TimeSetting.tm_mon,alarm1TimeSetting.tm_year ,alarm1TimeSetting.tm_wday,alarm1TimeSetting.tm_yday,alarm1TimeSetting.tm_isdst };
			X12RtcSetAlarm(1, &alarmTime1, flag1);
			printf("[Alarm] alarm 1 snoozed \n");
		}
		threeTimes++;
	}
}
void setVolume(struct Alarm *a, int vol)
{
    a->volume = vol;
}

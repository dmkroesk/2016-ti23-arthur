#include <stdio.h>
#include <string.h>

#include "menu.h"
#include "firstsetup.h"
#include "EEPROMHandler.h"
#include "dateTime.h"
#include "keyboard.h"
#include "alarmMenu.h"
#include "lcd.h"
#include "audioMenu.h"
#include "radioMenu.h"
#include "rss.h"
#include "StreamWork.h"

/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/
	
enum { alarmeringEnum, settingsEnum, radioEnum, rssEnum } scrollMenuIndex;
int isLCDrefreshed = 0;

/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
void drawLCD(void);
void goIntoScrollMenu(void);
void scrollMenuLeft(void);
void scrollMenuRight(void);

//verschillende menu's
/**
De onderstaande functies bepalen de controls voor elk menu
*/
void setupMenu(int input);		
void klokMenu(int input);	
void scrollMenu(int input);		
void alarmeringMenu(int input);
void radioMenu(int input);
void settingsMenu(int input);
void audioMenuItem(int input);
void rssMenu(int input);

void reset(void);
/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

void initMenu(void)
{
	int isClear;								
	scrollMenuIndex = alarmeringEnum;

	printf("checking memory for timezone\n");
	isClear = readEEPROM();
	if(isClear == 0)
	{
		printf("Timezone found\n");
		stateMenu = klokMenu;
		dateTimeInit();
	}else
	{
		printf("No Timezone found on EEPROM\n");
		stateMenu = setupMenu;
	}
	clearScreen();
	drawLCD();
	initAudioMenu();
}

void drawLCD()
{
	if(stateMenu == setupMenu)
	{
		drawUTC();
	}
	else if(stateMenu == klokMenu)
	{
		drawDateTime();
	}
	else if(stateMenu == scrollMenu)
	{
		if(scrollMenuIndex == alarmeringEnum)
		{
			printString("Alarmering");
		}
		else if(scrollMenuIndex == settingsEnum)
		{
			printString("Settings");
		}
		else if(scrollMenuIndex == radioEnum)
		{
			printString("Radio");
		}
		else if(scrollMenuIndex == rssEnum)
		{
			printString("RSS Feed");
		}
	}
	else if(stateMenu == alarmeringMenu){
		drawAlarm();
	}
	else if(stateMenu == settingsMenu){
		printString("Audio Settings");
	}
	else if(stateMenu == radioMenu)
	{
		drawRadio();
	}else if(stateMenu == rssMenu)
	{
		printRSS();
	}
}

void setupMenu(int input)
{
	isLCDrefreshed = 1;
	switch(input)
	{
	case KEY_OK:
		saveEEPROM();
		dateTimeInit();
		stateMenu = klokMenu;
		break;
	case KEY_UP:
		changeUtcUp();
		break;
	case KEY_DOWN:
		changeUtcDown();
		break;
	default:
		isLCDrefreshed = 0;
		break;
	}
	if (isLCDrefreshed == 1)
	{
		clearScreen();
		drawLCD();
	}
};

//klok
void klokMenu(int input)
{
	isLCDrefreshed = 1;
	switch(input)
	{
	case KEY_OK:
		stateMenu = scrollMenu;
		clearScreen();
		break;
	case KEY_UP:
		changeUtcUp();
		break;
	case KEY_DOWN:
		changeUtcDown();
		break;
	default:
		isLCDrefreshed = 0;
		break;
	}
	if (isLCDrefreshed == 1)
	{
		drawLCD();
	}
};

//menu
void scrollMenu(int input)
{
	isLCDrefreshed = 1;
	switch(input)
	{
	case KEY_OK:
		goIntoScrollMenu();
		break;
	case KEY_LEFT:
		scrollMenuLeft();
		break;
	case KEY_RIGHT:
		scrollMenuRight();
		break;
	case KEY_ESC:
		stateMenu = klokMenu;
		drawDateTimeComplete();
		isLCDrefreshed = 0;
		break;
	default:
		isLCDrefreshed = 0;
		break;
	}
	if (isLCDrefreshed == 1)
	{
		clearScreen();
		drawLCD();
	}
}

void goIntoScrollMenu()
{
	
	switch(scrollMenuIndex)
	{
		case alarmeringEnum:
			stateMenu = alarmeringMenu;
			break;
		case settingsEnum:
			stateMenu = settingsMenu;
			break;
		case radioEnum:
			radioOn=1;
			switchingStation=1;
			inRadioMenu=1;
			stateMenu = radioMenu;
			break;
		case rssEnum:
			stateMenu = rssMenu;
			break;
	}
}

void scrollMenuRight()
{

	if (scrollMenuIndex < 3)
	{
		scrollMenuIndex++;
	}
	else
	{
		scrollMenuIndex-=3;
	}
	//scrollMenuIndex++;
	//if(scrollMenuIndex > 1){ scrollMenuIndex = 0;}
}

void scrollMenuLeft()
{
	if (scrollMenuIndex > 0)
	{
		scrollMenuIndex--;
	}
	else
	{
		scrollMenuIndex+=3;
	}
	//scrollMenuIndex--;
	//if(scrollMenuIndex < 0){ scrollMenuIndex = 1;}
}
void radioMenu(int input)
{
	radioInput(input);
	switch(input)
	{
		case KEY_ESC:
			isLCDrefreshed = 1;
			radioOn=0;
			stopStream();
			inRadioMenu=0;
			stateMenu = scrollMenu;
		break;
		default:
			isLCDrefreshed = 1;
		break;
	}
	
	clearScreen();
	drawLCD();
}


void alarmeringMenu(int input)
{
	alarmInput(input);
	switch(input)
	{
		case KEY_ESC:
			isLCDrefreshed = 1;
			stateMenu = scrollMenu;
		break;
		default:
			isLCDrefreshed = 0;
		break;
	}
	
	clearScreen();
	drawLCD();
	
}
void settingsMenu(int input)
{
	isLCDrefreshed = 1;
	switch(input)
	{
	case KEY_OK:
		stateMenu = audioMenuItem;
		audioMenu(1000); //calles for a first refresh
		isLCDrefreshed = 0;
		break;
	case KEY_LEFT:
		break;
	case KEY_RIGHT:
		break;
	case KEY_ESC:
		stateMenu = scrollMenu;
		break;
	default:
		isLCDrefreshed = 0;
		break;
	}
	if (isLCDrefreshed == 1)
	{
		clearScreen();
		drawLCD();
	}
}
void audioMenuItem(int input)
{
	audioMenu(input);
	if (input == KEY_ESC && exitReady() == 1)
	{
		stateMenu = settingsMenu;
		stateMenu(KEY_RIGHT);
	}
}

void rssMenu(int input)
{
	switch(input)
	{
        case KEY_OK:

            break;
		case KEY_ESC:
			isLCDrefreshed = 1;
			stateMenu = scrollMenu;
			break;
		default:
			isLCDrefreshed = 0;
			break;
	}
	clearScreen();
	drawLCD();
}

void refresh()
{
	if(stateMenu == klokMenu) drawLCD();
	
}
void reset()
{
	stateMenu = setupMenu;
}

#define LOG_MODULE  LOG_RSS_MODULE


#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <dev/board.h>
#include <dev/debug.h>
#include <dev/nicrtl.h>
#include <sys/confnet.h>
#include <sys/timer.h>
#include <sys/thread.h>

#include <arpa/inet.h>
#include <net/route.h>
#include <pro/dhcp.h>
#include <pro/sntp.h>


#include <sys/socket.h>
#include <io.h>

#include <time.h>
#include <rss.h>
#include "strtok_r.h"
#include "lcd.h"
#include "display.h"

/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */


/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
FILE *rss;
TCPSOCKET *sock;
char rssInfo[2000];
char information[1000];
char* newsItemString;
int gotRSS = 0;

/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

int initRSS()
{
	gotRSS = 0;
	sock = NutTcpCreateSocket();
	if( NutTcpConnect(sock, inet_addr("146.185.137.179"), 80) )
	{
		printf("\nCould not TPC-Connect to RSSFeedServer\n");

		return 1;
	}else{
		printf("\nConnected to server 146.185.137.179:80\n");
		gotRSS = 1;
		return 0;
	}
}

void getRSSFeed(){
		char *data;
		rss = _fdopen( (int) sock, "r+b" );
		fprintf(rss, "GET /getrss.php %s HTTP/1.0\r\n", "/");
		fprintf(rss, "Host: %s\r\n", "146.185.137.179");
		fprintf(rss, "Connection: close\r\n\r\n");
		fflush(rss);

		data = (char *) malloc(1024 * sizeof (char));
		while (fgets(data, 1024, rss)) {
			if (0 == *data)
				break;
		}
		strcat(rssInfo, data);
		printf("%s\n", rssInfo);
		free(data);
}

void printRSS(){
	if(!gotRSS){
		setRSSInfo("Geen RSS beschikbaar...");
		displayRSS();
	}else {
		displayRSS();
	}
}

void setRSSInfo(char* tmp_information)
{
	strcpy(rssInfo, tmp_information);
}

void displayRSS()
{
	printString(rssInfo);
}

#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "log.h"
#include "firstsetup.h"
#include "led.h"
#include "lcd.h"
#include <keyboard.h>


#include "rtc.h"
#include <dev/board.h>
#include <stdio.h>
#include <io.h>
#include "EEPROMHandler.h"
#include "StreamWork.h"
#include <time.h>
#include "dateTime.h"
#include "alarm.h"
#include "radioMenu.h"


enum { THREEFM, QMUSIC, PRCNL } stations;


void initRadio(void)
{
	switchingStation=1;
	radioOn=0;
	inRadioMenu=0;
}

void quitRadio(void)
{
	
}

void printMetadata(void)
{
	setCursorPos(1,0);
	printString(songtitle);
}

void drawRadio(void)
{
	switch(currentStation)
	{
		case THREEFM:
		setCursorPos(0,6);
		printString("3FM");
		//setCursorPos(1,0);
		//printString(songtitle);
		break;
		case QMUSIC:
		setCursorPos(0,5);
		printString("QMUSIC");
		//setCursorPos(1,0);
		//printString(songtitle);
		break;
		case PRCNL:
		setCursorPos(0,5);
		printString("100%NL");
		//setCursorPos(1,0);
		//printString(songtitle);
		break;
		default:
		printString("ERR");
		break;
		
	}
	printf("in radio menu : %s \n",songtitle);
}

void radioInput(int input)
{
	switch(input)
	{
		case KEY_LEFT:
		if(currentStation>0)
		{
			currentStation--;
		}
		else{
			currentStation+=2;
		}
		stopStream();
		switchingStation=1;
		printf("switching radio\n");
		break;
		case KEY_RIGHT:
		if(currentStation<2)
		{
			currentStation++;
		}
		else{
			currentStation-=2;
		}
		stopStream();
		switchingStation=1;
		printf("switching radio\n");
		break;
	}
	
	
}

